export default defineAppConfig({
  pages: [
    "pages/home/index",
    "pages/index/index",
    "pages/login/index",
    "pages/my/index",
  ],
  window: {
    backgroundTextStyle: "light",
    navigationBarBackgroundColor: "#00aeec",
    navigationBarTitleText: "默认标题",
    navigationBarTextStyle: "black",
  },
  subPackages: [],
  tabBar: {
    color: "#000",
    selectedColor: "#00aeec",
    backgroundColor: "#fff",
    borderStyle: "black",
    list: [
      {
        pagePath: "pages/home/index",
        text: "首页",
        iconPath: "./static/tabbar/home.png",
        selectedIconPath: "./static/tabbar/home_select.png",
      },
      {
        pagePath: "pages/my/index",
        text: "我的",
        iconPath: "./static/tabbar/my.png",
        selectedIconPath: "./static/tabbar/my_select.png",
      },
    ]
  },
});
