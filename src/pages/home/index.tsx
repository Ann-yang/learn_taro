import { View, Text } from "@tarojs/components";
import { useLoad } from "@tarojs/taro";
import "./index.scss";

export default () => {
  useLoad(() => {
    console.log("page loaded");
  });

  return (
    <View className="className">
      <Text>home</Text>
    </View>
  );
};