import { View, Text } from "@tarojs/components";
import Taro, { useLoad } from "@tarojs/taro";
import "./index.scss";

export default function Index() {
  useLoad(() => {
    console.log("Page loaded.");
  });

  return (
    <View className="container">
      <Text onClick={() => Taro.navigateTo({ url: "/pages/login/index" })}>登录</Text>
      <Text onClick={() => Taro.switchTab({ url: "/pages/home/index" })}>首页</Text>
      <Text onClick={() => Taro.switchTab({ url: "/pages/my/index" })}>我的</Text>
    </View>
  );
}
